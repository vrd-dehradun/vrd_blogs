<!DOCTYPE html>
<html>
<head>
	<title>Media Section</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    
  	<link rel="stylesheet" href="../assets/css/normalize.css">
    <link rel="stylesheet" href="../assets/css/font-awesome.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/templatemo-style.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <script src="../assets/js/vendor/modernizr-2.6.2.min.js"></script>

    <script type="text/javascript">
        function getUrl(num){
            a = document.getElementById("mediaImage"+num)
            e = document.getElementById("mydata")
            e.setAttribute("value",a.src)
            e.hidden=false
            e.select()
            e.setSelectionRange(0, 99999)
            document.execCommand("copy")
            e.hidden=true
        }
    </script>

</head>
<body>

    <input type="text" id="mydata" value="clipboard data" hidden/>

	<div class="container my-5">
		<h3 class="text-center text-success h1"><strong>Media Section</strong></h3>
        <div class="row">
            <div class="col-md-12">
                <button id = "newImage" class="btn btn-success btn-block">Upload</button>
            </div>
        </div>

        <div id="imageModal" class="modal">
            <div class="modal-content">
                <span class="close">&times;</span>
                <div class="modal-header">
                    <h2 style="color:black;">Upload Image</h2>
                </div>
                <form action="upload.php" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <input class="form-control" type="file" name="fileToUpload" id="fileToUpload">
                        </div>
                        <div class="col-md-6">
                            <input class="btn btn-primary btn-block" type="submit" value="Upload Image" name="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>

	</div>

    <div class="main-posts">
        <div class="container">
            <div class="row">
                <div class="blog-masonry masonry-true">
                    <?php
                        $files = glob("../media/*.*");
                        for ($i=0; $i<count($files); $i++)
                        {
                            $image = $files[$i];
                            $supported_file = array(
                                    'gif',
                                    'jpg',
                                    'jpeg',
                                    'png'
                            );

                            $ext = strtolower(pathinfo($image, PATHINFO_EXTENSION));
                            if (in_array($ext, $supported_file)) {
                    ?>

                    <div class="post-masonry col-md-4 col-sm-6">
                        <div class="post-thumb">
                            <img id="mediaImage<?=$i?>" src="<?=$image?>" alt="">
                            <div class="title-over">
                                <h4><a href="#"><?=basename($image)?></a></h4>
                            </div>
                            <div class="post-hover text-center">
                                <div class="inside">
                                    <i class="fa fa-check-square"></i>
                                    <span class="date"><?=date("d-m-Y")?></span>
                                    <h4 onclick="getUrl(<?=$i?>)"><a href="#"><?=basename($image)?></a></h4>
                                    <p>This is a test paragraph</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                            } else {
                                continue;
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="../assets/js/vendor/jquery-1.10.2.min.js"></script>
    <script src="../assets/js/min/plugins.min.js"></script>
    <!-- <script src="../assets/js/min/main.min.js"></script> -->
    <script type="text/javascript">
        // Get the modal
        var modal = document.getElementById("imageModal");

        // Get the button that opens the modal
        var btn = document.getElementById("newImage");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on the button, open the modal
        btn.onclick = function() {
        modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>

</body>
</html>