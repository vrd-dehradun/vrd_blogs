<!DOCTYPE html>
<html lang="en">
<head>

	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WXFGQ7Z');</script>
	<title>VRD BLOG</title>
	<link rel="icon" href="LOGO.png" type="image/gif">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

	<!-- <link href="https://fonts.googleapis.com/css2?family=Corben&display=swap" rel="stylesheet">-->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap" rel="stylesheet">

	<script src="https://cdn.jsdelivr.net/gh/loadingio/transition.css@v2.0.0/dist/transition.min.css"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>

	<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<link rel="stylesheet" type="text/css" href="css/style2.css">

	<!-- <link rel="stylesheet" type="text/css" href="css/stylethree.css"> -->
	<style>

	.contactusspan{
		margin-left:20px; 
		font-size: 15px; 
		padding-bottom: 10px;
	}

	.contactusicon{
		font-size: 28px;
		color: white;
	}
	</style>

</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WXFGQ7Z"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="container-fluid c1 text-center svgmobile">
		<svg viewBox="0 0 500 500" class="viewBox">
			<a href="our-work.html"style="text-decoration: none; font-weight: normal;">
				<circle cx="100" cy="10" r="50" stroke="" fill="white" class="first"></circle>
				<text x="100" y="15" text-anchor="middle" font-size="18" fill="black">our
					<tspan x="100" y="40" font-size="18">Work</tspan>
				</text>
			</a>
			<a href="https://www.virtualrealdesign.com/">
				<circle cx="250" cy="10" r="90" fill="white" class="second"></circle>
				<image x="210" y="-10" width="90" height="95" xlink:href="VRD_logo.jpg"></image>
			</a>
			<a href="why-vrd.html" style="text-decoration: none; font-weight: normal;">
				<circle cx="400" cy="10" r="50" stroke="" fill="white" class="first"> </circle>
				<text x="400" y="15" text-anchor="middle" font-size="18" fill="black">why
					<tspan x="400" y="40" font-size="18">VRD</tspan>
				</text>
			</a>
		</svg>
		
	</div>


	<section class="image-section-for-blogone">

	<?php
		$root_url = $_SERVER['DOCUMENT_ROOT'];
		if($_COOKIE['islogin'] == 'true'){
	?>
			<a href="../logout.php" class="btn btn-info pull-right font-weight-bold" style="margin-top: 10px; margin-right: 10px;">Logout</a>
	<?php
		} else {
	?>
			<div class="container-fluid c1 text-center svgdesktop">
			
			<svg viewBox="0 0 500 500" class="viewBox">
				<a href="our-work.html"style="text-decoration: none; font-weight: normal;">
					<circle cx="100" cy="10" r="30" stroke="" fill="white" class="first"></circle>
	
					<text x="100" y="10" text-anchor="middle" font-size="8" fill="black">our
						<tspan x="100" y="20" font-size="8">Work</tspan>
					</text>
				</a>
				<a href="https://www.virtualrealdesign.com/">
					<circle cx="250" cy="15" r="70" fill="white" class="second"></circle>
					<image x="210" y="-10" width="80" height="75" xlink:href="VRD_logo.jpg"></image>
				</a>
				 <a href="why-vrd.html" style="text-decoration: none; font-weight: normal;">
					<circle cx="400" cy="10" r="30" stroke="" fill="white" class="first"> </circle>
	
					<text x="400" y="10" text-anchor="middle" font-size="8" fill="black">why
						<tspan x="400" y="20" font-size="8">VRD</tspan>
					</text>
	
				</a>
	
			</svg>
		
		</div>
	</section>
	<?php
		}
	?>

	<section class="image-section-for-blogone">
		<h2 class="text-center index" style=" color: white; font-weight: bold; padding:12px; background: linear-gradient(to left,#00BE96,#00BEFF); ">
			List Of BLOG 
			
			<a href="../login.php" class="btn btn-info pull-right font-weight-bold" style="position: relative; float: left;">Login</a>
		</h2>

		<?php
			include "$root_url/vrd_blogs/api/connectdb.php";
			include "$root_url/vrd_blogs/api/blogs.php";
			$actual_link = "http://$_SERVER[HTTP_HOST]";
			$db = new ConnectDB();
			$b = new Blogs($db->getdb());
			$resab = $b->get_all_blogs();
			$respb = $b->get_published_blogs();
			if($_COOKIE['user_type'] == 'ADMIN' && $_COOKIE['islogin'] == 'true'){
				for($i=0;$i<count($resab);$i++){
		?>
					<section style="position: relative;">
						<table class="table table-striped">
							<thead class="text-center">
								<tr>
									<th>Blog Image</th>
									<th>Blog Heading</th>
									<th>Blog Slug</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								<tr>
									<td style="width: 20%; text-align: center;">
										<img src="<?=$actual_link.'/vrd_blogs'.$resab[$i]['image']?>" width="80%" class="img-responsive" style="padding: 48px 0px;" />
									</td>

									<td style="width: 20%; text-align: center;">
										<!-- <a href = "index?id=<?=$resab[$i]['blog_id']?>&slug=<?=$resab[$i]['slug']?>"> 
											<h3 class="py-5"><?=$resab[$i]['blog_heading']?></h3>
										</a> -->
										
										<h3 class="py-5"><?=$resab[$i]['blog_heading']?></h3>
									<?php
										if($resab[$i]['published'] == 'N' && $_COOKIE['user_type'] == 'ADMIN'){
											echo "Unpublished";
										} else if($resab[$i]['published'] == 'Y' && $_COOKIE['user_type'] == 'ADMIN'){
											echo "Published";
										}
									?>
									</td>

									<td style="text-align: center; width: 40%;">
										<p style="padding: 48px 0px; font-size:14px; font-weight: bold;">
											<?php
												echo $resab[$i]['slug'];
											?>
										</p>
										
									</td>
									
									<td style="text-align: center; width:10%;">
										<h4 style="padding: 48px 0px;">
											<?php
												if($resab[$i]['published'] == 'Y'){
													echo 'Active';
												}
											?>
										</h4>
										
									</td>

									<td style="text-align: center; width: 10%">
										<h4 style="padding: 48px 0px;">

											<a href = "index.php?id=<?=$resab[$i]['blog_id']?>" class="btn btn-primary btn-infomration mb-3"> Visit Blog </a>

											<a href = "edit.php?blog_id=<?=$resab[$i]['blog_id']?>" class="btn btn-primary btn-danger mb-3"> Edit Blog </a>

											<a href = "post/<?=$resab[$i]['slug']?>" class="btn btn-success mb-3" target="_blank"> Preview </a>
										</h4>

										<h3 style="line-height: 200px;">
											
										</h3>
										
									</td>
								</tr>
							</thead>
							
						</table>
					</section>

		<?php
				}
			} else {
				if($respb == 1){
		?>
					<div class="container my-5">
						<div class="row" style="border: 2px solid black;">
							<div class="col-lg-8 col-md-8 col-12 py-3">
								Sorry There are No Blogs
							</div>
						</div>
					</div>
		<?php
				} else {
					for($i=0;$i<count($respb);$i++){
		?>
						<section style="position: relative;">
						<table class="table table-striped">
							<thead class="text-center">
								<tr>
									<th>Blog Image</th>
									<th>Blog Heading</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
								<tr>
									<td style="width: 20%; text-align: center;"><img src="<?=$actual_link.'/vrd_blogs'.$respb[$i]['image']?>" width="40%" class="img-responsive" style="padding: 48px 0px;" /></td>
									<td style="width: 40%; text-align: center;">
										<!-- <a href = "index?id=<?=$respb[$i]['blog_id']?>">  -->
										<h3 class="py-5" style="font-size: 16px; font-weight: bold;"><?=$respb[$i]['blog_heading']?></h3>
									</a>
									<!-- <?php
										if($respb[$i]['published'] == 'N' && $_COOKIE['user_type'] == 'ADMIN'){
											echo "Unpublished";
										} else if($respb[$i]['published'] == 'Y' && $_COOKIE['user_type'] == 'ADMIN'){
											echo "Published";
										}
									?> -->
									</td>
									<td style="text-align: center; width:20%">
										<h4 style="padding: 48px 0px;">
											<?php
												if($respb[$i]['published'] == 'Y'){
													echo 'Active';
												}
											?>
										</h4>
									</td>

									<td style="text-align: center; width: 20%">
										<h4 style="padding: 48px 0px;">
										<?php
											$blog_slug = $respb[$i]['slug'];
										?>
										
										<!-- <form action="post/<?=$blog_slug?>" method="post">

											<input type="hidden" name="blog_slug" id="blog_slug" value="<?=$respb[$i]['slug']?>">

											
										</form> -->

										<a href = "post/<?=$blog_slug?>" class="btn btn-primary btn-information" value="Visit Blog"> Visit Blog </a>

										<!-- <a href = "index?id=<?=$respb[$i]['blog_id']?>" class="btn btn-primary btn-infomration"> Visit Blog </a> -->
										</h4>
									</td>
								</tr>
							</thead>
							
						</table>
					</section>
		<?php
					}
				}
			}
		?>
	</section>

	<section class="footerpart" style="padding: 25px; font-size: 15px; line-height: 37px;">
		<div class="container-fluid">
			<div class="row pt-4">
				<div class="col-lg-3 col-md-3 col-6">
					<h5 style="font-weight: bold;">WEB</h5>
					<div>
					<ul style="list-style-type: none;">
						<li style="cursor: pointer; ">Landing Page Design</li>
						<li style="cursor: pointer; ">Brochure Website Design</li>
						<li style="cursor: pointer;">Website ReDesign</li>
						<li style="cursor: pointer; ">Business Website Design</li>
						<li style="cursor: pointer; ">Event Website Design</li>
						<li style="cursor: pointer; ">Workshop Website Design</li>
						<li style="cursor: pointer;">Responsive Web Design</li>
						<li style="cursor: pointer;">Mobile Website Design</li>
						<li style="cursor: pointer; ">Content Management System</li>
						<li style="cursor: pointer; ">E-Commerce Web Design</li>
						<li style="cursor: pointer; ">Frameworks</li>
						<li style="cursor: pointer; ">UI Design</li>
						<li style="cursor: pointer;">Email-Design</li>
						<li style="cursor: pointer; ">EDM Design</li>
						<li style="cursor: pointer;">Newsletter Design</li>
						<li style="cursor: pointer; ">Web Hosting & Emails</li>
						
					</ul>
				</div>
				</div>
				<div class="col-lg-3 col-md-3 col-6">
					<h5 style="font-weight: bold;">Graphics</h5>
					<ul style="list-style-type: none;">
						<li style="cursor: pointer; ">Corporate Identity</li>
						<li style="cursor: pointer; ">Logo Design</li>
						<li style="cursor: pointer;">Business Card Design</li>
						<li style="cursor: pointer; ">Stationary Design</li>
						<li style="cursor: pointer; ">Brochure Design</li>
						<li style="cursor: pointer; ">Resume Design</li>
						<li style="cursor: pointer;">Catalogue Design</li>
						<li style="cursor: pointer; ">Leaflet Design</li>
						<li style="cursor: pointer; ">Poster Design</li>
						<li style="cursor: pointer; ">Packaging Design</li>
						<li style="cursor: pointer; ">Book Cover Design</li>
						<li style="cursor: pointer; ">Calender Design</li>
						<li style="cursor: pointer; ">Magazine Design</li>
						<li style="cursor: pointer;">Invitation Design</li>
						<li style="cursor: pointer; ">Sticker Design</li>
						<li style="cursor: pointer;">Icon Design</li>
					</ul>
					
				</div>
				<div class="col-lg-3 col-md-3 col-6">
					<h5 style="font-weight: bold;">MOBILE</h5>
					<ul style="list-style-type: none;">
						<li style="cursor: pointer; ">App Design(UI/UX)</li>
						<li style="cursor: pointer; ">Mobile Websites</li>
						
					</ul>

					<h5 style="font-weight: bold;">MARKETING</h5>
					<ul style="list-style-type: none;">
						<li style="cursor: pointer;">Social Media Marketing(SMM)</li>
						<li style="cursor: pointer; ">Pay-Per-Click Advertising(PPC)</li>
						<li style="cursor: pointer; ">Search Engine Optimization(SEO)</li>
						<li style="cursor: pointer; ">E-mail Campaign</li>
						<li style="cursor: pointer; ">Analytics & Conversions</li>
						
					</ul>

					<h5 style="font-weight: bold;">EVENT</h5>
					<ul style="list-style-type: none;">
						<li style="cursor: pointer; ">Event Package</li>
						<li style="cursor: pointer;">Workshop Package</li>
					</ul>
					
				</div>
				<div class="col-lg-3 col-md-3 col-6">
					<h5 style="font-weight: bold;">Quick Links</h5>
					<ul style="list-style-type: none;">
						<li><a href="http://virtualrealdesign.com/" style="text-decoration: none; color: white;">Home</a></li>
						<li><a href="why-vrd.html" style="text-decoration: none; color: white;">About Us</a></li>
						<li><a href="our-services.html" style="text-decoration: none; color: white;">Services</a></li>
						<li><a href="our-work.html" style="text-decoration: none; color: white;">Work</a></li>
						<li><a href="career.html" style="text-decoration: none; color: white;">Work with us</a></li>
						</ul>
						<div data-toggle="modal" data-target="#demo21" style="cursor: pointer; margin-top: -15px;">Contact</div>
							<div class="modal" id="demo21">

						  		<div class="modal-dialog modal-dialog-centered">
						  			<div class="modal-content">

						  				<div class="modal-body">
						  					<div class="container">
						  						<div class="row">
						  							<div class="col-lg-8 col-md-8 offset-md-2 col-12">
						  								<h3 class="text-center"> get in touch </h3>
											<form action="contact.php" method="post">
											<div class="form-group">
												<input type="text" name="full_name" class="form-control form-control-sm" placeholder="Full Name" style="background-color: #f2f2f2;" autocomplete="off" required />
												
											</div>

											<div class="form-group">
												<input type="text" name="cname" class="form-control form-control-sm" placeholder="Company Name" style="background-color: #f2f2f2;" autocomplete="off" required />
												
											</div>

											<div class="form-group">
												<input type="email" name="email" class="form-control form-control-sm" autocomplete="off" placeholder="Email" style="background-color: #f2f2f2;" required />
												
											</div>
										
											<div class="form-group">
												<input type="tel" name="contact_no" class="form-control form-control-sm" placeholder="Contact Number" style="background-color: #f2f2f2;" autocomplete="off" required />
												
											</div>

											<div class="form-group">
												<textarea rows="5" cols="50" class="form-control form-control-sm" name="msg" placeholder="Message" style="background-color: #f2f2f2;" ></textarea>
												
											</div>

											<div class="g-recaptcha" data-sitekey="6LeqgiIaAAAAALKH4IhOuL6seHlDLfoU2eRKf5Bl" ></div>

											<div class="form-group text-center">
												<input type="submit" name="submit" class="submitclick" value="SUBMIT">
												
											</div>
										</form>
												
									</div>
										
						  						</div>
						  						
						  					</div>
						  					
						  				</div>
						  				
						  			</div>
								  			
						  		</div>
						  		
						  	</div>
						<ul style="list-style-type: none;">
						<li><a href="team.html" style="text-decoration: none; color: white;">Team</a></li>
						<li><a href="privacy_policy.html" style="text-decoration: none; color: white;">Privacy Policy</a></li>
						<li><a href="terms-conditions.html" style="text-decoration: none; color: white;">Terms & Conditions</a></li>
						<li><a href="refund-policy.html" style="text-decoration: none; color: white; ">Cancellation/Refund Policy</a></li>
						
					</ul>

					<h5 style="font-weight: bold;">Contact Us</h5>
					<i class="fa fa-mobile contactusicon"></i><span class="contactusspan" >+91 7899365102</span><br/>
					<i class="fa fa-envelope text-white"></i><span class="contactusspan">enquiry@virtualrealdsign.com</span>
					<br/>
					<div class="d-flex">
						<i class="fa fa-map-marker align-self-start contactusicon"></i><span class="contactusspan">7/5, old survey road, Dehradun, Uttrakhand</span><br/>
						
					</div>
					<h5 style="font-weight: bold;">FOLLOW US</h5>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-3">
							<a href="https://www.facebook.com/virtualrealdesign" target="_blank" style="color: white;"><i class="fa fa-facebook-official fa-2x text-white"></i></a>
							
						</div>	
						<div class="col-lg-3 col-md-3 col-3">
							<a href="https://www.instagram.com/virtualrealdesign/" target="_blank" style="color: white;"><i class="fa fa-instagram fa-2x text-white"></i></a>
							
						</div>
						<div class="col-lg-3 col-md-3 col-3">
							<a href="https://www.linkedin.com/company/virtualrealdesign/" target="_blank" style="color: white"><i class="fa fa-linkedin-square fa-2x text-white"></i></a>							
						</div>		

						<div class="col-lg-3 col-md-3 col-3">
							<a href="https://twitter.com/vrd40?lang=en" target="_blank" style="color: white;"><i class="fa fa-twitter fa-2x text-white"></i></a>						
						</div>		

					</div>
					
					
				</div>
				
			</div>
		</div>
	</section>

	<!-- Footer Part for Mobile view -->

	<section class="footerpart1">
		<div class="container">
	
			<div class="svgmobile1">
							<svg viewBox="0 0 60 60">
								<a href="https://www.virtualrealdesign.com/"><circle cx="30" cy="2" r="18" fill="white" stroke="none" class=""></circle>
								<image x="20" y="-7" width="20px" height="30" xlink:href="VRD_logo.jpg"></image></a>

							</svg>
			
				</div>

			<div class="row" style="margin-top: -200px;">
				<div class="col-lg-8 col-md-8 col-12">
					<div class="row">			
						<div class="col-lg-1 col-md-1 col-1 offset-1">
							
						</div>
						<div class="col-lg-2 col-md-2 col-2">
								<a href="https://www.facebook.com/virtualrealdesign" target="_blank"><i class="fa fa-facebook-official fa-2x fontawesomecolor fontawesomecolor1"></i></a>

						</div>
						<div class="col-lg-2 col-md-2 col-2">
							<a href="https://www.instagram.com/virtualrealdesign/" target="_blank"><i class="fa fa-instagram fa-2x fontawesomecolor fontawesomecolor1"></i></a>
						</div>
						<div class="col-lg-2 col-md-2 col-2">
							<a href="https://www.linkedin.com/company/virtualrealdesign/" target="_blank"><i class="fa fa-linkedin-square fa-2x fontawesomecolor fontawesomecolor1"></i></a>

						</div>
						<div class="col-lg-2 col-md-2 col-2">
							<a href="https://twitter.com/vrd40?lang=en" target="_blank"><i class="fa fa-twitter fa-2x fontawesomecolor fontawesomecolor1"></i></a>
														
						</div>
						<div class="col-lg-2 col-md-2 col-2">
					
						</div>
						<div class="col-lg-1 col-md-1 col-1">
							
						</div>
					</div>
				</div>

			</div>

			<div class="row mt-5">
				<div class="col-lg-4 col-md-4 col-4">
					<h6 class="font-weight-bold">Links</h6>
					<ul style="list-style-type: none;">
						<li><a href="http://virtualrealdesign.com/" style="color: white; text-decoration: none;">Home</a>
						</li>
						<li><a href="why-vrd.html" style="color: white; text-decoration: none; ">About US</a></li>
						<li><a href="our-services.html" style="color: white; text-decoration: none;">Services</a></li>
						<li><a href="our-work.html" style="color: white; text-decoration: none;">Work</a></li>
						<li><a href="career.html" style="color: white; text-decoration: none;">Work&nbsp;with&nbsp;us</a></li>
					</ul>
					<div data-toggle="modal" data-target="#demo22" style="cursor: pointer; margin-top: -15px;">Contact</div>
							<div class="modal" id="demo22">

						  		<div class="modal-dialog modal-dialog-centered">
						  			<div class="modal-content">

						  				<div class="modal-body">
						  					<div class="container">
						  						<div class="row">
						  							<div class="col-lg-8 col-md-8 offset-md-2 col-12">
						  								<h3 class="text-center"> get in touch </h3>
											<form action="contact.php" method="post">
											<div class="form-group">
												<input type="text" name="full_name" class="form-control form-control-sm" placeholder="Full Name" style="background-color: #f2f2f2;" autocomplete="off" required />
												
											</div>

											<div class="form-group">
												<input type="text" name="cname" class="form-control form-control-sm" placeholder="Company Name" style="background-color: #f2f2f2;" autocomplete="off" required />
												
											</div>

											<div class="form-group">
												<input type="email" name="email" class="form-control form-control-sm" autocomplete="off" placeholder="Email" style="background-color: #f2f2f2;" required />
												
											</div>
										
											<div class="form-group">
												<input type="tel" name="contact_no" class="form-control form-control-sm" placeholder="Contact Number" style="background-color: #f2f2f2;" autocomplete="off" required />
												
											</div>

											<div class="form-group">
												<textarea rows="5" cols="50" class="form-control form-control-sm" name="msg" placeholder="Message" style="background-color: #f2f2f2;" ></textarea>
												
											</div>

										
											
											<div class="form-group text-center">
												<input type="submit" name="submit" class="submitclick" value="SUBMIT">
												
											</div>
										</form>
												
									</div>
										
						  						</div>
						  						
						  					</div>
						  					
						  				</div>
						  				
						  			</div>
								  			
						  		</div>
						  		
						  	</div>

						  	<ul style="list-style-type: none;">
						<li><a href="team.html" style="text-decoration: none; color: white;">Team</a></li>
						<li><a href="privacy_policy.html" style="text-decoration: none; color: white; ">Privacy Policy</a></li>
						<li><a href="terms-conditions.html" style="text-decoration: none; color: white; ">Terms & Conditions</a></li>
						<li><a href="refund-policy.html" style="text-decoration: none; color: white;">Cancellation/Refund Policy</a></li>
					</ul>
					
					
				</div>
				<div class="col-lg-8 col-md-8 col-8">
					<h5 class="font-weight-bold">Contact Us</h5>
					<div class="line">
						<div>
							<i class="fa fa-mobile fa-2x fontawesomecolor"><span class="size">+91 7899365102</span> </i>
						</div>
						<div>
							<i class="fa fa-envelope fontawesomecolor"><span class="size">enquiry@virtualrealdesign.com</span></i>
						</div>
						<div class="pt-2">
							<i class="d-flex fa fa-map-marker fa-2x fontawesomecolor"><span class="size">7/5, old survey road, Dehradun, Uttrakhand.</span></i>
						</div>
					</div>
					
				</div>
				
			</div>
			
		</div>
	</section>

<script>
	window.onload = function() {
var $recaptcha = document.querySelector('#g-recaptcha-response');

if($recaptcha) {
$recaptcha.setAttribute("required", "required");
}
};
</script>

</body>
</html>