<!DOCTYPE html>
<html>
<head>
	<title>Vrd Blogs Users</title>
	 <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
<?php
	if(isset($_GET['msg'])){
		if($_GET['msg'] == 'Invalid'){
			echo "<h2>Invalid Email ID or Password</h2>";
		}
	}
?>
	<div class="container my-5">
		<h3 class="text-center text-success my-4">Login</h3>
		<div class="row">
			<div class="col-lg-10 col-md-10 col-12 offset-md-1">
				<form action="submit.php" method="post">

					<input type="hidden" id="form_name" name="form_name" value="login"/>
					
					<div class="form-group">
						<label>Email ID</label>
						<input type="email" name="email_id" class="form-control" placeholder="Enter email id">
					</div>

					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control" placeholder="Enter password">
					</div>
					
					<div>
						<input type="submit" value="Login" name="submitbtn" class="btn btn-primary btn-lg mt-4">
					</div>
				</form>				
			</div>
		</div>
	</div>
</body>
</html>