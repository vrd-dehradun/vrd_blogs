<!DOCTYPE html>
<html>
<?php
	if($_COOKIE['user_type'] == 'ADMIN') {
?>
<head>
	<title>Vrd Blogs Category</title>
	 <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>

	<div class="container my-5">
		<h3 class="text-center text-success my-4">VRD Blogs Category</h3>
		<div class="row">
			<div class="col-lg-10 col-md-10 col-12 offset-md-1">
				<form id="category_form" action="submit.php" method="post">
					<input type="hidden" id="form_name" name="form_name" value="category"/>
					<div class="form-group">
						<label>Category Name</label>
						<input type="text" name="category_name" class="form-control" placeholder="Category Name">
					</div>
					
					<div class="form-group">
						<label>Blog Counts</label>
						<input type="text" name="blog_counts" class="form-control" placeholder="Blog Counts">
					</div>
				
					<div>
						<input type="submit" name="submitbtn" class="btn btn-primary btn-lg mt-4">
					</div>
				</form>
			</div>
		</div>
	</div>

</body>
<?php
	} else {
		echo "<h2> 404 Not Found </h2>";
	}
?>
</html>