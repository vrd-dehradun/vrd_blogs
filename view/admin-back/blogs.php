<!DOCTYPE html>
<html>
<?php
	if($_COOKIE['user_type'] == 'ADMIN') {
?>
<head>
	<title>Vrd Blogs</title>
	 <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- <script src="https://cdn.ckeditor.com/ckeditor5/10.0.1/classic/ckeditor.js"></script> -->
	<!-- <script src="https://cdn.ckeditor.com/4.16.0/standard-all/ckeditor.js"></script> -->
	<script src="http://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>

	<script type="text/javascript">
		function submitForm(){
			authorelement = document.getElementById("author_id")
			categoryelement = document.getElementById("category_id")

			if(authorelement.value == "0" || categoryelement.value == "0"){
				alert('required')
			}else{
				document.getElementById("blog_form").submit();
			}			
		}
	</script>


</head>
<body>
	<?php
		$root_url = $_SERVER['DOCUMENT_ROOT'];
		include "$root_url/vrd_blogs/api/connectdb.php";
		$cdb = new ConnectDB();
	?>
	<div class="container my-5">
		<h3 class="text-center text-success my-4">VRD Blogs</h3>
		<div class="row">
			<div class="col-lg-10 col-md-10 col-12 offset-md-1">
				<form id="blog_form" action = "submit.php" method="post" enctype="multipart/form-data">
					<input type="hidden" id="form_name" name="form_name" value="blogs"/>
					<div class="form-group">
						<label>Author Name</label>
						<select class="form-control" id="author_id" name="author_id">
							<option value="0">Choose Author</option>

							<?php
								include "$root_url/vrd_blogs/api/users.php";
								$u = new Users($cdb->getdb());
								$res = $u->get_user();
								if($u->status_code == 200){
									for($i=0;$i<count($res);$i++){
							?>
										<option value="<?=$res[$i]['user_id']?>"><?=$res[$i]['user_name']?></option>
							<?php
									}
								}

							?>
						</select>
						<!-- <input type="number" name="author_id" class="form-control" placeholder="Enter Author Name"> -->
					</div>

					<div class="form-group">
						<label>Blog Heading</label>
						<input type="text" id="blog_heading" name="blog_heading" class="form-control" placeholder="Blog Heading">
					</div>

					<div>
						<label>Blog Content</label>
						<textarea name="blog_content" id="blog_content" class="form-control ckeditor">
							<p>This is Blog content.</p>
						</textarea>
					</div>

					<div class="form-group">
						<label>Choose Category</label>
						<!-- <input type="number" name="category_id" class="form-control" placeholder="Category"> -->
						<select class="form-control" id="category_id" name="category_id">
							<option value="0">Choose Category</option>
							<?php
								include "$root_url/vrd_blogs/api/category.php";
								$c = new Category($cdb->getdb());
								$resc = $c->get_category();
								if($c->status_code == 200){
									for($i=0;$i<count($resc);$i++){
							?>
										<option value="<?=$resc[$i]['category_id']?>"><?=$resc[$i]['category_name']?></option>
							<?php
									}
								}

							?>
						</select>
					</div>

					<div class="form-group">
						<label>Featured Image</label>
						<input type="file" name="featured_image" class="form-control" placeholder="Select Image">
					</div>
					
					<input type="button" value="Submit" onclick="submitForm()" name="submitbtn" class="btn btn-primary btn-lg">
				</form>
			</div>
		</div>
	</div>

	<script>

		CKEDITOR.replace( 'content', {
			height: 300,
			filebrowserUploadUrl: "ckupload.php"
		});
		// CKEDITOR.addCss('.cke_editable { font-size: 15px; padding: 2em; }');

		// CKEDITOR.replace('editor', {
		// toolbar: [{
		// 	name: 'document',
		// 	items: ['Print']
		// 	},
		// 	{
		// 	name: 'clipboard',
		// 	items: ['Undo', 'Redo']
		// 	},
		// 	{
		// 	name: 'styles',
		// 	items: ['Format', 'Font', 'FontSize']
		// 	},
		// 	{
		// 	name: 'colors',
		// 	items: ['TextColor', 'BGColor']
		// 	},
		// 	{
		// 	name: 'align',
		// 	items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
		// 	},
		// 	'/',
		// 	{
		// 	name: 'basicstyles',
		// 	items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']
		// 	},
		// 	{
		// 	name: 'links',
		// 	items: ['Link', 'Unlink']
		// 	},
		// 	{
		// 	name: 'paragraph',
		// 	items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
		// 	},
		// 	{
		// 	name: 'insert',
		// 	items: ['Image', 'Table']
		// 	},
		// 	{
		// 	name: 'tools',
		// 	items: ['Maximize']
		// 	},
		// 	{
		// 	name: 'editing',
		// 	items: ['Scayt']
		// 	}
		// ],

		// extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',

		// // Adding drag and drop image upload.
		// extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
		// uploadUrl: 'upload.php',

		// // Configure your file manager integration. This example uses CKFinder 3 for PHP.
		// filebrowserBrowseUrl: 'media.php',
		// filebrowserImageBrowseUrl: 'media.php',
		// filebrowserUploadUrl: 'upload.php',
		// filebrowserImageUploadUrl: 'upload.php',

		// height: 300,

		// removeDialogTabs: 'image:advanced;link:advanced'
		// });
	</script>

	<!-- <script>
		ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
	</script> -->

<!-- <script>
	ClassicEditor
    .create( editorElement, {
        ckfinder: {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
        }
    }            .create( document.querySelector( '#editor' ) )
    
                
    .catch(  error => {
                console.error( error );
            });
</script> -->	

</body>
<?php
	} else {
		echo "<h2> 404 Not Found </h2>";
	}
?>
</html>