<!DOCTYPE html>
<html>
<?php
	$root_url = $_SERVER['DOCUMENT_ROOT'];
	if($_COOKIE['user_type'] == 'ADMIN') {
?>
<head>
	<title>Vrd Blogs Users</title>
	 <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
	<?php
		include "$root_url/vrd_blogs/api/connectdb.php";
		$cdb = new ConnectDB();
	?>
	<div class="container my-5">
		<h3 class="text-center text-success my-4">VRD Blogs Users</h3>
		<div class="row">
			<div class="col-lg-10 col-md-10 col-12 offset-md-1">
				<form action="submit.php" method="post">

					<input type="hidden" id="form_name" name="form_name" value="users"/>

					<div class="form-group">
						<label>User Name</label>
						<input type="text" name="user_name" class="form-control" placeholder="User Name">
					</div>
					
					<div class="form-group">
						<label>Email ID</label>
						<input type="email" name="email_id" class="form-control" placeholder="Enter email id">
					</div>

					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control" placeholder="Enter password">
					</div>

					<div class="form-group">
						<label>User Type</label>
						<select class="form-control" id="user_type" name="user_type">
							<option value="0">Choose User Type</option>

							<?php
								include "$root_url/vrd_blogs/api/user_type.php";
								$ut = new UserType($cdb->getdb());
								$rest = $ut->get_user_type();
								if($ut->status_code == 200){
									for($i=0;$i<count($rest);$i++){
							?>
										<option value="<?=$rest[$i]['type_id']?>"><?=$rest[$i]['type_name']?></option>
							<?php
									}
								}

							?>
						</select>
					</div>
					
					<div class="form-group">
						<label>Blog Counts</label>
						<input type="number" name="blog_count" class="form-control" placeholder="Enter Blog Count">
					</div>
					
					<div>
						<input type="submit" name="submitbtn" class="btn btn-primary btn-lg mt-4">
					</div>
				</form>				
			</div>
		</div>
	</div>

</body>
<?php
	} else {
		echo "<h2> 404 Not Found </h2>";
	}
?>
</html>