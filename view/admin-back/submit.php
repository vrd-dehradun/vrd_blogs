<?php
    // print_r($_POST['users_form']);
    $root_url = $_SERVER['DOCUMENT_ROOT'];
    include "$root_url/vrd_blogs/api/connectdb.php";
	$cdb = new ConnectDB();
    if($cdb->isconnected == true){
        if($_POST['form_name'] == 'users'){
            include "$root_url/vrd_blogs/api/users.php";
            $u = new Users($cdb->getdb());
    
            $res = $u->create_user($_POST['user_name'], $_POST['email_id'], $_POST['password'], $_POST['blog_count'],'Y', $_POST['user_type']);
    
            if($u->status_code == 200){
                header('Location: users.php');
            }else if($u->status_code == 500){
                header('Location: 500.php');
            }
        } else if($_POST['form_name'] == 'blogs'){
            include "$root_url/vrd_blogs/api/blogs.php";
            include "$root_url/vrd_blogs/locus/utils/fileupload.php";
    
            $b = new Blogs($cdb->getdb());
            $fu = new FileUpload('../../media/blog_thumbnails/', 'featured_image');
            if($fu->upload() == 200){
    
                $blog_data = base64_encode($_POST['blog_content']);
    
                $res = $b->create_blogs($_POST['author_id'], $blog_data, $_POST['category_id'], '/media/blog_thumbnails/'.$fu->getFileName(),$_POST['blog_heading']);
    
                if($b->status_code == 200){
                    header('Location: blogs.php');
                } else if($u->status_code == 500) {
                    header('Location: 500.php');
                }
            }
        }else if($_POST['form_name'] == 'category'){
            include "$root_url/vrd_blogs/api/category.php";
            $u = new Category($cdb->getdb());
    
            $res = $u->create_category($_POST['category_name'], $_POST['blog_counts'], 'Y');
    
            if($u->status_code == 200){
                header('Location: category.php');
            }else if($u->status_code == 500){
                header('Location: 500.php');
            }
        } else if ($_GET['action'] == 'allow') {
            include "$root_url/vrd_blogs/api/blogs.php";
            $u = new Blogs($cdb->getdb());
    
            $res = $u->blogs_published($_GET['blog_id']);
    
            if($u->status_code == 200){
                header('Location: ../blog/list.php');
            }else if($u->status_code == 500){
                header('Location: 500.php');
            }
        }
    }
?>