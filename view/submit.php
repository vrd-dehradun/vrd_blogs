<?php
    $root_url = $_SERVER['DOCUMENT_ROOT'];
    include "$root_url/vrd_blogs/api/connectdb.php";
	$cdb = new ConnectDB();

    include "$root_url/vrd_blogs/api/login.php";
    include "$root_url/vrd_blogs/api/user_type.php";

    $u = new Login($cdb->getdb());
    $ut = new UserType($cdb->getdb());

    $res = $u->login_user($_POST['email_id'], $_POST['password']);
    // print_r($ut->get_user_type_by_id($res[0]['user_type']));
    // exit;

    if($u->status_code == 200){
        setcookie('islogin', 'true', time() + (86400 * 30), "/"); // 1 Day
        setcookie('userid', $res[0]['user_id'], time() + (86400 * 30), "/");

        $user_type_res = $ut->get_user_type_by_id($res[0]['user_type']);

        if($user_type_res[0]['type_name'] == 'ADMIN'){
	    setcookie('user_type', 'ADMIN', time() + (86400 * 30), "/");
            header('Location: admin/dashboard.php');
        } else {
            header('Location: blog/list.php');
        }
    }else if($u->status_code == 500){
        header('Location: 500.php');
    } else if($u->status_code == 404){
        header('Location: login.php?msg=Invalid');
    }
?>