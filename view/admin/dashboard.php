﻿<!DOCTYPE html>
<html lang="en">
<?php
    $root_url = $_SERVER['DOCUMENT_ROOT'];

    if($_COOKIE['islogin'] != "true"){
        header('Location: ../login.php');
    } else {
        if($_COOKIE['user_type'] == 'ADMIN'){
?>
<head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>VRD</title>
        <link type="text/css" href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="assets/css/theme.css" rel="stylesheet">
        <link type="text/css" href="assets/images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html"> Virtual Real Design </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav pull-right">
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="assets/images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="../logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="dashboard.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li><a href="users.php"><i class="menu-icon icon-bullhorn"></i>Add New User </a>
                                </li>
                                <li><a href="blogs.php"><i class="menu-icon icon-inbox"></i>Add New Blog </a></li>
                                <li><a href="category.php"><i class="menu-icon icon-tasks"></i>Add New Category </a></li>
                                <li><a href="../blog/list"><i class="menu-icon icon-tasks"></i>View Blogs </a></li>
                                <li><a href="../media.php"><i class="menu-icon icon-tasks"></i>Media</a></li>
                                
                            </ul>
                        </div>
                    </div>

                    <?php
                        include "$root_url/vrd_blogs/api/blogs.php";
                        include "$root_url/vrd_blogs/api/users.php";
                        include "$root_url/vrd_blogs/api/connectdb.php";
	                    $cdb = new ConnectDB();
                        $b = new Blogs($cdb->getdb());
                        $u = new Users($cdb->getdb());
                        $resb = $b->get_blog_count();
                        $resu = $u->get_user_count();
                    ?>

                    <div class="span9">
                        <div class="content">
                            <div class="btn-controls">
                                <div class="btn-box-row row-fluid">
                                    <a href="#" class="btn-box big span4"><i class=" icon-random"></i><b><?=$resb[0]['total']?></b>
                                        <p class="text-muted">Total Blogs</p>
                                    </a><a href="#" class="btn-box big span4"><i class="icon-user"></i><b><?=$resu[0]['total']?></b>
                                        <p class="text-muted">
                                            Total Users</p>
                                    </a><a href="#" class="btn-box big span4"><i class="icon-money"></i><b>15,152</b>
                                        <p class="text-muted">
                                            Profit</p>
                                    </a>
                                </div>
                                <div class="btn-box-row row-fluid">
                                    <div class="span8">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <a href="#" class="btn-box small span4"><i class="icon-envelope"></i><b>Messages</b>
                                                </a><a href="#" class="btn-box small span4"><i class="icon-group"></i><b>Clients</b>
                                                </a><a href="#" class="btn-box small span4"><i class="icon-exchange"></i><b>Expenses</b>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <a href="#" class="btn-box small span4"><i class="icon-save"></i><b>Total Sales</b>
                                                </a><a href="#" class="btn-box small span4"><i class="icon-bullhorn"></i><b>Social Feed</b>
                                                </a><a href="#" class="btn-box small span4"><i class="icon-sort-down"></i><b>Bounce
                                                    Rate</b> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="widget widget-usage unstyled span4">
                                        <li>
                                            <p>
                                                <strong>Windows 8</strong> <span class="pull-right small muted">78%</span>
                                            </p>
                                            <div class="progress tight">
                                                <div class="bar" style="width: 78%;">
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <p>
                                                <strong>Mac</strong> <span class="pull-right small muted">56%</span>
                                            </p>
                                            <div class="progress tight">
                                                <div class="bar bar-success" style="width: 56%;">
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <p>
                                                <strong>Linux</strong> <span class="pull-right small muted">44%</span>
                                            </p>
                                            <div class="progress tight">
                                                <div class="bar bar-warning" style="width: 44%;">
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <p>
                                                <strong>iPhone</strong> <span class="pull-right small muted">67%</span>
                                            </p>
                                            <div class="progress tight">
                                                <div class="bar bar-danger" style="width: 67%;">
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2021 Virtual Real Design - virtualrealdesign.com </b>All Rights Reserved.
            </div>
        </div>
        <script src="assets/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="assets/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="assets/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="assets/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="assets/scripts/common.js" type="text/javascript"></script>
      
    </body>
<?php
        } else {
            echo "<h2> 404 Not Found </h2>";
        }
    }
?>
    </html>