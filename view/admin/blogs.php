<!DOCTYPE html>
<html>
<?php
	if($_COOKIE['user_type'] == 'ADMIN') {
?>
<head>
	<title>Vrd Blogs</title>
	 <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

  	<link type="text/css" href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="assets/css/theme.css" rel="stylesheet">
    <link type="text/css" href="assets/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>


    <!-- <script src="https://cdn.ckeditor.com/ckeditor5/10.0.1/classic/ckeditor.js"></script> -->
	<!-- <script src="https://cdn.ckeditor.com/4.16.0/standard-all/ckeditor.js"></script> -->
	<script src="http://cdn.ckeditor.com/4.6.2/standard-all/ckeditor.js"></script>

	<script type="text/javascript">
		function submitForm(){
			authorelement = document.getElementById("author_id")
			categoryelement = document.getElementById("category_id")

			if(authorelement.value == "0" || categoryelement.value == "0"){
				alert('required')
			}else{
				document.getElementById("blog_form").submit();
			}			
		}

		function checkSlug(resp){
			if(resp.value == 'sample' || resp.value == 'render'){
				alert('This slug value is not allowed')
			}

			$.get("submit.php?slug="+resp.value, function(data, status){
				if(data == 'EXIST') {
					document.getElementById('slug_err').innerHTML = 'Blog Slug already exist'
				}
			});
		}
	</script>


</head>
<body>
	<?php
		$root_url = $_SERVER['DOCUMENT_ROOT'];
		include "$root_url/vrd_blogs/api/connectdb.php";
		$cdb = new ConnectDB(); 
	?>



	<div class="navbar navbar-fixed-top " >
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                    <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html"> Virtual Real Design </a>
                <div class="nav-collapse collapse navbar-inverse-collapse">
                    <ul class="nav pull-right">
                        <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="assets/images/user.png" class="nav-avatar" />
                            <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Your Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="../logout.php">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.nav-collapse -->
            </div>
        </div>
        <!-- /navbar-inner -->
    </div>

        <div class="wrapper">
            <div class="container">
                <div class="row">
                	<div class="span3">
                        <h2 style="background-color: #fff; color: #2d2b32; text-align: center; text-transform: uppercase; box-shadow: 2px 2px 5px rgba(45, 43, 50, 0.5), -2px -2px 5px rgba(45, 43, 50, 0.5); padding: 10px;">Admin Panel</h2>
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="dashboard.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li><a href="users.php"><i class="menu-icon icon-bullhorn"></i>Add New User </a>
                                </li>
                                <li><a href="blogs.php"><i class="menu-icon icon-inbox"></i>Add New Blog </a></li>
                                <li><a href="category.php"><i class="menu-icon icon-tasks"></i>Add New Category </a></li>
                                <li><a href="../blog/list"><i class="menu-icon icon-tasks"></i>View Blogs </a></li>
                                <li><a href="../media.php"><i class="menu-icon icon-tasks"></i>Media</a></li>
                            </ul>
                        </div>
                    </div>
                    
		            <div class="span9">
		            	<h3 style="text-align: center; font-size: 32px; background-color: #fff; padding: 10px; color: #2d2b32; box-shadow: 2px 2px 5px rgba(45, 43, 50, 0.5), -2px -2px 5px rgba(45, 43, 50, 0.5);">VRD Blogs</h3>
			
						<form id="blog_form" action = "submit.php" method="post" enctype="multipart/form-data">
							<input type="hidden" id="form_name" name="form_name" value="blogs"/>
							<div class="form-group">
								<label>Author Name</label>
								<select id="author_id" name="author_id" style="width: 100%">
									<option value="0">Choose Author</option>
									<?php
										include "$root_url/vrd_blogs/api/users.php";
										$u = new Users($cdb->getdb());
										$res = $u->get_user();
										if($u->status_code == 200){
											for($i=0;$i<count($res);$i++){
									?>
												<option value="<?=$res[$i]['user_id']?>"><?=$res[$i]['user_name']?></option>
									<?php
											}
										}

									?>
								</select>
								<!-- <input type="number" name="author_id" class="form-control" placeholder="Enter Author Name"> -->
							</div>

							<div class="form-group">
							    <div style="width: 49%; float:left;">
								<label>Blog Heading</label>
								<input type="text" id="blog_heading" name="blog_heading" placeholder="Blog Heading" style="width: 100%; height: 30px;">
                                </div>
                                <div style="width: 49%; float:right;">
								<label>Slug</label>
								<input type="text" id="slug" name="slug" onblur="checkSlug(this)" placeholder="Blog Slug" style="width: 98%; height: 30px;">
								<label id="slug_err"> 
									<?php
										if($_GET['msg'] == 'EXIST'){
									?>
											Blog with this slug already exist
									<?php		
										}
									?>
								</label>
								</div>
							</div>

							<div>
								<label>Blog Content</label>
								<textarea name="blog_content" id="blog_content" class="form-control ckeditor">
									<p>This is Blog content.</p>
								</textarea>
							</div>

							<div class="form-group">
								<label>Meta Description</label>
								<textarea name="meta_description" id="meta_description" class="form-control" style="width:99%;"> Meta Description here</textarea>
							</div>

							<div class="form-group">
								<label>Choose Category</label>
								<!-- <input type="number" name="category_id" class="form-control" placeholder="Category"> -->
								<select id="category_id" name="category_id" style="width: 100%;">
									<option value="0">Choose Category</option>
									<?php
										include "$root_url/vrd_blogs/api/category.php";
										$c = new Category($cdb->getdb());
										$resc = $c->get_category();
										if($c->status_code == 200){
											for($i=0;$i<count($resc);$i++){
									?>
												<option value="<?=$resc[$i]['category_id']?>"><?=$resc[$i]['category_name']?></option>
									<?php
											}
										}

									?>
								</select>
							</div>

							<div class="form-group">
								<label>Featured Image</label>
								<input type="file" name="featured_image" placeholder="Select Image" style="width: 100%; background-color: white;">
							</div>
							
							<input type="button" value="Submit" onclick="submitForm()" name="submitbtn" class="btn btn-primary btn-lg">
						</form>			
					</div>
				</div>
			</div>
		</div>

		<div class="footer" >
            <div class="container">
                <b class="copyright">&copy; 2021 Virtual Real Design - virtualrealdesign.com </b>All Rights Reserved.
            </div>
        </div>

        <script src="assets/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="assets/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="assets/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="assets/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="assets/scripts/common.js" type="text/javascript"></script>

	<script>

		CKEDITOR.replace( 'blog_content', {
			height: 300,
			filebrowserUploadUrl: "ckupload.php"
		});
		// CKEDITOR.addCss('.cke_editable { font-size: 15px; padding: 2em; }');

		// CKEDITOR.replace('editor', {
		// toolbar: [{
		// 	name: 'document',
		// 	items: ['Print']
		// 	},
		// 	{
		// 	name: 'clipboard',
		// 	items: ['Undo', 'Redo']
		// 	},
		// 	{
		// 	name: 'styles',
		// 	items: ['Format', 'Font', 'FontSize']
		// 	},
		// 	{
		// 	name: 'colors',
		// 	items: ['TextColor', 'BGColor']
		// 	},
		// 	{
		// 	name: 'align',
		// 	items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
		// 	},
		// 	'/',
		// 	{
		// 	name: 'basicstyles',
		// 	items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting']
		// 	},
		// 	{
		// 	name: 'links',
		// 	items: ['Link', 'Unlink']
		// 	},
		// 	{
		// 	name: 'paragraph',
		// 	items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']
		// 	},
		// 	{
		// 	name: 'insert',
		// 	items: ['Image', 'Table']
		// 	},
		// 	{
		// 	name: 'tools',
		// 	items: ['Maximize']
		// 	},
		// 	{
		// 	name: 'editing',
		// 	items: ['Scayt']
		// 	}
		// ],

		// extraAllowedContent: 'h3{clear};h2{line-height};h2 h3{margin-left,margin-top}',

		// // Adding drag and drop image upload.
		// extraPlugins: 'print,format,font,colorbutton,justify,uploadimage',
		// uploadUrl: 'upload.php',

		// // Configure your file manager integration. This example uses CKFinder 3 for PHP.
		// filebrowserBrowseUrl: 'media.php',
		// filebrowserImageBrowseUrl: 'media.php',
		// filebrowserUploadUrl: 'upload.php',
		// filebrowserImageUploadUrl: 'upload.php',

		// height: 300,

		// removeDialogTabs: 'image:advanced;link:advanced'
		// });
	</script>

	<!-- <script>
		ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
                console.error( error );
            } );
	</script> -->

<!-- <script>
	ClassicEditor
    .create( editorElement, {
        ckfinder: {
            uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json'
        }
    }            .create( document.querySelector( '#editor' ) )
    
                
    .catch(  error => {
                console.error( error );
            });
</script> -->	

</body>
<?php
	} else {
		echo "<h2> 404 Not Found </h2>";
	}
?>
</html>