<?php
    // print_r($_POST['users_form']);
    $root_url = $_SERVER['DOCUMENT_ROOT'];
    include "$root_url/vrd_blogs/api/connectdb.php";
	$cdb = new ConnectDB();
    if($cdb->isconnected == true){
        if(isset($_GET['slug'])){
            include "$root_url/vrd_blogs/api/blogs.php";
    
            $b = new Blogs($cdb->getdb());

            $res = $b->get_blog_from_slug($_GET['slug']);

            if(isset($res[0]['blog_id'])){
                echo "EXIST";
            }
        }

        if($_POST['form_name'] == 'users'){
            include "$root_url/vrd_blogs/api/users.php";
            $u = new Users($cdb->getdb());
    
            $res = $u->create_user($_POST['user_name'], $_POST['email_id'], $_POST['password'], $_POST['blog_count'],'Y', $_POST['user_type']);
    
            if($u->status_code == 200){
                header('Location: users.php');
            }else if($u->status_code == 500){
                header('Location: ../500.php');
            }
        } else if($_POST['form_name'] == 'blogs'){
            include "$root_url/vrd_blogs/api/blogs.php";
            include "$root_url/vrd_blogs/locus/utils/fileupload.php";
    
            $b = new Blogs($cdb->getdb());

            $res = $b->get_blog_from_slug($_POST['slug']);

            if(isset($res[0]['blog_id'])){
                header("Location: blogs.php?msg=EXIST");
                exit;
            }
            
            $fu = new FileUpload('../../media/blog_thumbnails/', 'featured_image');

            if($fu->upload() == 200) {

                $file_path = $_SERVER['DOCUMENT_ROOT'].'/vrd_blogs/view/blog/post/'.$_POST['slug'].'.php';
                $post_file_path = $_SERVER['DOCUMENT_ROOT'].'/vrd_blogs/view/blog/post';
    
                $blog_data = base64_encode($_POST['blog_content']);
                $meta_description = base64_encode($_POST['meta_description']);
    
                $res = $b->create_blogs($_POST['author_id'], $blog_data, $_POST['category_id'], '/media/blog_thumbnails/'.$fu->getFileName(),$_POST['blog_heading'], $_POST['slug'], $meta_description);
    
                if($b->status_code == 200){
                    if (touch($file_path)) {
                        echo $file_path . " modification time has been changed to present time";
                        if(copy("$post_file_path/sample.php", "$file_path") == 1) {
                            header('Location: blogs.php');
                        } else {
                            header('Location: ../500.php');
                        }
                    } else {
                        echo "Sorry, could not change modification time of " . $file_path;
                        $resd = $b->delete_blog($_POST['slug']);

                        if($b->status_code == 200){
                            header('Location: blogs.php?msg=ERR');
                        }
                        
                        header('Location: ../500.php');
                    }
                } else if($b->status_code == 500) {
                    header('Location: ../500.php');
                }
            }
        } else if($_POST['form_name'] == 'category'){
            include "$root_url/vrd_blogs/api/category.php";
            $u = new Category($cdb->getdb());
    
            $res = $u->create_category($_POST['category_name'], $_POST['blog_counts'], 'Y');
    
            if($u->status_code == 200){
                header('Location: category.php');
            }else if($u->status_code == 500){
                header('Location: ../500.php');
            }
        } else if ($_GET['action'] == 'allow') {
            include "$root_url/vrd_blogs/api/blogs.php";
            $u = new Blogs($cdb->getdb());
    
            $res = $u->blogs_published($_GET['blog_id']);
    
            if($u->status_code == 200){
                header('Location: ../blog/list.php');
            }else if($u->status_code == 500){
                header('Location: ../500.php');
            }
        } else if ($_POST['form_name'] == 'edit_blogs') {
            
            include "$root_url/vrd_blogs/api/blogs.php";
            include "$root_url/vrd_blogs/locus/utils/fileupload.php";

            $file_path = $_SERVER['DOCUMENT_ROOT'].'/vrd_blogs/view/blog/post/'.$_POST['slug'].'.php';
            $post_file_path = $_SERVER['DOCUMENT_ROOT'].'/vrd_blogs/view/blog/post';
    
            $b = new Blogs($cdb->getdb());
            $blog_data = base64_encode($_POST['blog_content']);
            $meta_description = base64_encode($_POST['meta_description']);
            
            if (!empty($_FILES['edit_featured_image']['name'])) {
                $fu = new FileUpload('../../media/blog_thumbnails/', 'edit_featured_image');

                if($fu->upload() == 200) {
                    $res = $b->edit_blog($_POST['blog_id'], $_POST['blog_heading'], $_COOKIE['userid'] , $blog_data, $_POST['slug'], $meta_description, '/media/blog_thumbnails/'.$fu->getFileName());
                }
            } else {
                $res = $b->edit_blog($_POST['blog_id'], $_POST['blog_heading'], $_COOKIE['userid'] , $blog_data, $_POST['slug'], $meta_description, $_POST['featured_image']);
            }

            if($b->status_code == 200){
                if (touch($file_path)) {
                    echo $file_path . "modification time has been changed to present time";
                    if(copy("$post_file_path/sample.php", "$file_path") == 1) {
                        header('Location: blogs.php');
                    } else {
                        header('Location: ../500.php');
                    }
                } else {
                    echo "Sorry, could not change modification time of " . $file_path;
                    $resd = $b->delete_blog($_POST['slug']);

                    if($b->status_code == 200){
                        header('Location: blogs.php?msg=ERR');
                    }
                    
                    header('Location: ../500.php');
                }

                header('Location: ../blog/index.php?id='.$_POST['blog_id']);
            } else if($b->status_code == 500) {
                header('Location: ../500.php');
            }

        }
    }
?>