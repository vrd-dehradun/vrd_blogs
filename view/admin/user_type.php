<!DOCTYPE html>
<html>
<head>
	<title>Vrd Blogs Users</title>
	 <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>

	<div class="container my-5">
		<h3 class="text-center text-success my-4">VRD User Type</h3>
		<div class="row">
			<div class="col-lg-10 col-md-10 col-12 offset-md-1">
				<div class="form-group">
					<label>Type ID</label>
					<input type="number" name="type_id" class="form-control" placeholder="Enter Type of ID">
				</div>
				<div class="form-group">
					<label>Type name</label>
					<input type="text" name="type_name" class="form-control" placeholder="Enter type name">
				</div>			
				<div>
					<input type="submit" name="submitbtn" class="btn btn-primary btn-lg mt-4">
				</div>	
													
			</div>			
		</div>
	</div>

</body>
</html>