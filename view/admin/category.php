<!DOCTYPE html>
<html>
<?php
	if($_COOKIE['user_type'] == 'ADMIN') {
?>
<head>
	<title>Vrd Blogs Category</title>
	<!--  <meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

  	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>VRD</title>
        <link type="text/css" href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="assets/css/theme.css" rel="stylesheet">
        <link type="text/css" href="assets/images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>

</head>
<body>

	<div class="navbar navbar-fixed-top " >
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.html"> Virtual Real Design </a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav pull-right">
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="assets/images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="../logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->  		

        <div class="wrapper">
            <div class="container">
                <div class="row">
                	<div class="span3">
                        <h2 style="background-color: #fff; color: #2d2b32; text-align: center; text-transform: uppercase; box-shadow: 2px 2px 5px rgba(45, 43, 50, 0.5), -2px -2px 5px rgba(45, 43, 50, 0.5); padding: 10px;">Admin Panel</h2>
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="dashboard.php"><i class="menu-icon icon-dashboard"></i>Dashboard
                                </a></li>
                                <li><a href="users.php"><i class="menu-icon icon-bullhorn"></i>Add New User </a>
                                </li>
                                <li><a href="blogs.php"><i class="menu-icon icon-inbox"></i>Add New Blog </a></li>
                                <li><a href="category.php"><i class="menu-icon icon-tasks"></i>Add New Category </a></li>
                                <li><a href="../blog/list"><i class="menu-icon icon-tasks"></i>View Blogs </a></li>
                                <li><a href="../media.php"><i class="menu-icon icon-tasks"></i>Media</a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- <?php
                        // include "$root_url/vrd_blogs/api/blogs.php";
                        // include "$root_url/vrd_blogs/api/users.php";
                        // include "$root_url/vrd_blogs/api/connectdb.php";
	                    // $cdb = new ConnectDB();
                        // $b = new Blogs($cdb->getdb());
                        // $u = new Users($cdb->getdb());
                        // $resb = $b->get_blog_count();
                        // $resu = $u->get_user_count();
                    ?>
 -->
                    <div class="span9">
                    	<h3 style="text-align: center; font-size: 32px; background-color: #fff; padding: 10px; color: #2d2b32; box-shadow: 2px 2px 5px rgba(45, 43, 50, 0.5), -2px -2px 5px rgba(45, 43, 50, 0.5);">VRD Blogs Category</h3>
                    	<form id="category_form" action="submit.php" method="post" style="margin-top: 50px;">
					<input type="hidden" id="form_name" name="form_name" value="category"/>
					<div class="form-group">
						<label>Category Name</label>
						<input type="text" name="category_name" placeholder="Category Name" style="width: 100%">
					</div>

					<div class="form-group" style="margin-top: 20px;">
						<label>Blog Counts</label>
						<input type="text" name="blog_counts" placeholder="Blog Counts" style="width: 100%;">
					</div>
				
					<div>
						<input type="submit" name="submitbtn" class="btn btn-primary btn-lg" style="margin-top: 20px;">
					</div>
				</form>


                </div>
                </div>
            </div>
        </div>

        <div class="footer" >
            <div class="container">
                <b class="copyright">&copy; 2021 Virtual Real Design - virtualrealdesign.com </b>All Rights Reserved.
            </div>
        </div>



	<!-- <div class="container my-5">
		<h3 class="text-center text-success my-4">VRD Blogs Category</h3>
		<div class="row">
			<div class="col-lg-10 col-md-10 col-12 offset-md-1">
				<form id="category_form" action="submit.php" method="post">
					<input type="hidden" id="form_name" name="form_name" value="category"/>
					<div class="form-group">
						<label>Category Name</label>
						<input type="text" name="category_name" class="form-control" placeholder="Category Name">
					</div>
					
					<div class="form-group">
						<label>Blog Counts</label>
						<input type="text" name="blog_counts" class="form-control" placeholder="Blog Counts">
					</div>
				
					<div>
						<input type="submit" name="submitbtn" class="btn btn-primary btn-lg mt-4">
					</div>
				</form>
			</div>
		</div>
	</div> -->

	<script src="assets/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="assets/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="assets/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="assets/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="assets/scripts/common.js" type="text/javascript"></script>

</body>
<?php
	} else {
		echo "<h2> 404 Not Found </h2>";
	}
?>
</html>