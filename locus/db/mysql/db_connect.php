<?php
	include "db_credentials.php";

	class db_connect
	{
		var $conn;
		var $res;

		function __construct()
		{
			// $this->dbname = $dbname;

			$dbc = new db_credentials("vrd","vrd@1322","vrd_blogs");
			$username = $dbc->getuser();
			$password = $dbc->getpass();
			$dbname = $dbc->getdbname();

			$this->conn = mysqli_connect("localhost", $username, $password, $dbname);

			return $this->conn;
		}

		function execute($query)
		{
			$resultset = array();
			$res = mysqli_query($this->conn, $query);
			if($res)
			{
				if (mysqli_num_rows($res) > 0) {
				    while($row = mysqli_fetch_assoc($res)) {
						$resultset[] = $row;
				    }
					return $resultset;
				} 
				else {
				    return true;
				}
			}
			else
			{
				return false;
			}
		}

		function getColumns($table_name)
		{
			$sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table_name'";

			$result = mysqli_query($this->conn,$sql);
			if($result)
			{
				while($row = mysqli_fetch_array($result)){
				    $col[] = $row;
				}
				$columnArr = array_column($col, 'COLUMN_NAME');
				return $columnArr;
			}
			else
			{
				return false;
			}
		}
	}
?>