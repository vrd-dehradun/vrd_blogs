<?php
    class FileUpload{
        var $target_file;
        var $FileType;
        var $FileSize;
        var $html_element_name;
        var $statusCode;
        var $FileName;

        function __construct($dir, $html_element_name){
            $this->target_file = $dir . basename($_FILES[$html_element_name]["name"]);
            $this->FileType = strtolower(pathinfo($this->target_file,PATHINFO_EXTENSION));
            $this->FileSize = getimagesize($_FILES[$html_element_name]["tmp_name"]);
            $this->html_element_name = $html_element_name;
            $this->FileName = $_FILES[$html_element_name]["name"];
        }

        function getFileSize(){
            return $this->FileSize;
        }

        function getFileType(){
            return $this->FileType;
        }

        function getFileName(){
            return $this->FileName;
        }

        function upload(){
            if (move_uploaded_file($_FILES[$this->html_element_name]["tmp_name"], $this->target_file)) {
                $this->statusCode = 200;
            } else {
                $this->statusCode = 500;
            }
            return $this->statusCode;
        }
    }
?>