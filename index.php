<?php
    if(isset($_COOKIE['islogin'])){
        if($_COOKIE['islogin'] == 'true'){
            if($_COOKIE['user_type'] == 'ADMIN'){
                header('Location: view/admin/dashboard.php');
            } else {
                header('Location: view/blog/list.php');
            }
        } else {
            header('Location: view/blog/list');
        }
    } else {
        header('Location: view/blog/list');
    }
?>