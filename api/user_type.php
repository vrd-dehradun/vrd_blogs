<?php
    // include "../locus/db/mysql/db_connect.php";

    class UserType{
        var $isconnected;
        var $db;
        var $msg;
        var $status_code;
        var $return_string;

        function __construct($db){
            $this->db = $db;
        }

        function get_user_type(){
            $q = "select * from user_type";
            $res = $this->db->execute($q);
            
            if(is_null($res)){
                $this->msg = "USER NOT FOUND";
                $this->status_code = 404;
            }else{
                $this->msg = "USER FOUND";
                $this->status_code = 200;
            }
            return $res;
        }

        function get_user_type_by_id($type_id){
            $q = "select * from user_type where type_id = '$type_id'";
            $res = $this->db->execute($q);
            
            if(is_null($res)){
                $this->msg = "USER TYPE NOT FOUND";
                $this->status_code = 404;
            }else{
                $this->msg = "USER TYPE FOUND";
                $this->status_code = 200;
            }
            return $res;
        }

        function create_user_type($type_name){

            $q = "insert into users(user_name) values('$type_name')";

            $res = $this->db->execute($q);

            if(!$res){
                $this->msg = "USER TYPE NOT CREATED";
                $this->status_code = 500;
                // echo $this->msg;
            }else{
                $this->msg = "USER TYPE CREATED";
                $this->status_code = 200;
                // echo $this->msg;
            }
            return $res;
        }
    }
?>