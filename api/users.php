<?php
    // include "../locus/db/mysql/db_connect.php";

    class Users{
        var $isconnected;
        var $db;
        var $msg;
        var $status_code;
        var $return_string;

        function __construct($db){
            // $this->db = new db_connect();
            // if($this->db){
            //     $this->isconnected = true;
            // }else{
            //     $this->isconnected = false;
            // }
            // return $this->isconnected;
            $this->db = $db;
        }

        function get_user(){
            $q = "select * from users";
            $res = $this->db->execute($q);
            
            if(is_null($res)){
                $this->msg = "USER NOT FOUND";
                $this->status_code = 404;
            }else{
                $this->msg = "USER FOUND";
                $this->status_code = 200;
            }
            return $res;
        }

        function create_user($user_name, $email_id, $password, $blog_counts, $active, $user_type){
            $password = md5($password);

            $q = "insert into users(user_name, email_id, password, blog_counts, active, user_type) values('$user_name','$email_id','$password',$blog_counts,'$active','$user_type')";

            $res = $this->db->execute($q);

            if(!$res){
                $this->msg = "USER NOT CREATED";
                $this->status_code = 500;
                // echo $this->msg;
            }else{
                $this->msg = "USER CREATED";
                $this->status_code = 200;
                // echo $this->msg;
            }
            return $res;
        }

        function get_user_count(){
            $q="select count(*) as total from users";
            $res = $this->db->execute($q);

            if(!$res){
                $this->msg="BLOG NOT PUBLISHED";
                $this->status_code=500;
                // echo $this->msg;
            }else{
                $this->msg="BLOG PUBLISHD SUCCESSFULLY";
                $this->status_code = 200;
                // echo $this->msg;
            }
            return $res;
        }
    }

    // $u = new Users();
    // $u->get_user();
    // $u->create_user('Karuna', 'karuna@virtualrealdesign.com','karuna',3,'Y');
?>