<?php
    // include "../locus/db/mysql/db_connect.php";

    class Blogs{
        var $isconnected;
        var $db;
        var $msg;
        var $status_code;
        var $return_string;

        function __construct($db){
            // $this->db = new db_connect();
            // if($this->db){
            //     $this->isconnected = true;
            // }else{
            //     $this->isconnected = false;
            // }
            // return $this->isconnected;

            $this->db = $db;
        }

        function get_all_blogs(){
            $q = "select * from blogs order by published";
            $res = $this->db->execute($q);
            if(is_null($res)){
                $this->msg = "NO BLOGS TILL NOW";
                $this->status_code = 404;
            }else{
                $this->msg = "BLOGS FOUND";
                $this->status_code = 200;
            }
            return $res;
        }

        function get_blog($blog_id){
            $q = "select * from blogs where blog_id = '$blog_id'";
            $res = $this->db->execute($q);
            if(is_null($res)){
                $this->msg = "NO BLOGS TILL NOW";
                $this->status_code = 404;
            }else{
                $this->msg = "BLOGS FOUND";
                $this->status_code = 200;
            }
            return $res;
        }

        function get_blog_from_slug($slug){
            $q = "select * from blogs where slug = '$slug'";
            $res = $this->db->execute($q);
            if(is_null($res)){
                $this->msg = "NO BLOGS TILL NOW";
                $this->status_code = 404;
            }else{
                $this->msg = "BLOGS FOUND";
                $this->status_code = 200;
            }
            return $res;
        }

        function delete_blog($slug){
            $q = "delete from blogs where slug = '$slug'";
            $res = $this->db->execute($q);
            if(is_null($res)){
                $this->msg = "BLOG NOT FOUND";
                $this->status_code = 404;
            }else{
                $this->msg = "BLOGS DELETED";
                $this->status_code = 200;
            }
            return $res;
        }

        function create_blogs($author_id, $blog_content, $category_id, $image, $blog_heading, $slug, $meta_description){
            $q = "insert into blogs(author_id, blog_content, category_id, image, blog_heading, slug, meta_description) values('$author_id','$blog_content','$category_id','$image','$blog_heading','$slug', '$meta_description')"; 

            $res = $this->db->execute($q);

            if(!$res){
                $this->msg = "BLOG NOT CREATED";
                $this->status_code = 500;
                // echo $this->msg;
            }else{
                $this->msg = "BLOG CREATED";
                $this->status_code = 200;
                // echo $this->msg;
            }
            
            return $res;
        }

        function blogs_published($blog_id){
            $q="update blogs SET published='Y' WHERE blog_id=$blog_id";
            $res = $this->db->execute($q);

            if(!$res){
                $this->msg="BLOG NOT PUBLISHED";
                $this->status_code=500;
                // echo $this->msg;
            }else{
                $this->msg="BLOG PUBLISHD SUCCESSFULLY";
                $this->status_code = 200;
                // echo $this->msg;
            }
            return $res;
        }

        function get_published_blogs(){
            $q = "select * from blogs where published = 'Y' ";
            $res = $this->db->execute($q);
            if(is_null($res)){
                $this->msg = "NO BLOGS TILL NOW";
                $this->status_code = 404;
            }else{
                $this->msg = "BLOGS FOUND";
                $this->status_code = 200;
            }
            return $res;
        }

        function get_blog_count(){
            $q="select count(*) as total from blogs";
            $res = $this->db->execute($q);

            if(!$res){
                $this->msg="BLOG NOT PUBLISHED";
                $this->status_code=500;
                // echo $this->msg;
            } else {
                $this->msg="BLOG PUBLISHD SUCCESSFULLY";
                $this->status_code = 200;
                // echo $this->msg;
            }
            return $res;
        }

        function edit_blog($blog_id, $blog_heading, $user_id, $blog_content, $slug, $meta_description, $image){

            $q = "update blogs set blog_heading = '$blog_heading', blog_content = '$blog_content', edited_by = '$user_id', slug='$slug', meta_description='$meta_description', image='$image' where blog_id = '$blog_id' ";
            
            $res = $this->db->execute($q);

            if(!$res){
                $this->msg="BLOG NOT PUBLISHED";
                $this->status_code=500;
            } else {
                $this->msg="BLOG PUBLISHD SUCCESSFULLY";
                $this->status_code = 200;
            }
            return $res;
        }
    }

    // $u = new Blogs();
    // $u->get_all_blogs();
    // $u->create_user('Karuna', 'karuna@virtualrealdesign.com','karuna',3,'Y');
?>