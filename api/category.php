<?php
    // include "../locus/db/mysql/db_connect.php";

    class Category{
        var $isconnected;
        var $db;
        var $msg;
        var $status_code;
        var $return_string;

        function __construct($db){
            // $this->db = new db_connect();
            // if($this->db){
            //     $this->isconnected = true;
            // }else{
            //     $this->isconnected = false;
            // }
            // return $this->isconnected;

            $this->db = $db;
        }

        function get_category(){
            $q = "select * from category";
            $res = $this->db->execute($q);
            if(is_null($res)){
                $this->msg = "CATEGORY NOT FOUND";
                $this->status_code = 404;
            }else{
                $this->msg = "CATEGORY FOUND";
                $this->status_code = 200;
            }
            return $res;
        }

        function create_category($category_name, $blog_counts, $active){
            $q = "insert into category(category_name, blog_counts, active) values('$category_name','$blog_counts','$active')";

            $res = $this->db->execute($q);

            if(!$res){
                $this->msg = "CATEGORY NOT CREATED";
                $this->status_code = 500;
                echo $this->msg;
            }else{
                $this->msg = "CATEGORY CREATED";
                $this->status_code = 200;
                echo $this->msg;
            }
            return $res;
        }
    }

    // $u = new Category();
    // $u->get_category();
    // $u->create_category('Technology', 3,'Y');
?>