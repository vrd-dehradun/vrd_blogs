<?php
    $root_url = $_SERVER['DOCUMENT_ROOT'];
    include "$root_url/vrd_blogs/locus/db/mysql/db_connect.php";

    class ConnectDB {
        var $isconnected;
        var $db;

        function __construct(){
            $this->db = new db_connect();

            if($this->db){
                $this->isconnected = true;
            }else{
                $this->isconnected = false;
            }

            return $this->db;
        }

        function getdb(){
            return $this->db;
        }
    }
?>